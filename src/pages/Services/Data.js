export const homeObjOne = {
  primary: true,
  lightBg: false,
  imgStart: "",
  lightTextDesc: true,
  lightTopLine: true,
  buttonLabel: "Get Started",
  description:
    "We Help business owners increase their revenue.Our Team of unique specialist can help you achieve your Business goals.",
  headLine: "Lead Generation Specialist for Online Business",
  lightText: true,
  topLine: "Marketing Agency",
  img: require("../../images/svg-1.svg").default,
  start: "",
  alt: "Image",
};

export const homeObjTwo = {
  lightBg: true,
  primary: false,
  imgStart: "start",
  lightTextDesc: false,
  lightTopLine: false,
  buttonLabel: "Learn More",
  description:
    "We have you covered no matter where you are located. Over 140 locations worldwide to ensure you have access anytime",
  headLine: "Stay protected 24/7 anywhere anytime",
  lightText: false,
  topLine: "100% Secure",
  img: require("../../images/Profile1.jpeg").default,
  start: "true",
  alt: "Image",
};

export const homeObjThree = {
  primary: true,
  lightBg: false,
  imgStart: "",
  lightTextDesc: true,
  lightTopLine: true,
  buttonLabel: "Start Now",
  description:
    "Get everything set up and ready in under 10 minutes. All you need to do is add your information and you're ready to go.",
  headLine: "Super fast and simple onboarding process",
  lightText: true,
  topLine: "Easy Setup",
  img: require("../../images/svg-2.svg").default,
  start: "",
  alt: "Image",
};

export const homeObjFour = {
  primary: true,
  lightBg: false,
  imgStart: "start",
  lightTextDesc: true,
  lightTopLine: true,
  buttonLabel: "Sign Up Now",
  description:
    "Never ever have to worry again about saved reciepts. We store your data, so you can access it anytime.",
  headLine: "Every transaction is stored on our secure cloud database",
  lightText: true,
  topLine: "DATA ANALYTICS",
  img: require("../../images/svg-3.svg").default,
  start: "true",
  alt: "Image",
};
